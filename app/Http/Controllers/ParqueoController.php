<?php

namespace App\Http\Controllers;
use App\Models\Posicion;
use App\Models\Parqueo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class ParqueoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function listPosicionesLibres(){
        $posicion = new Posicion;
        $tipoVeh=$_GET['tipoVeh'];

        $posicionesLibres=$posicion->get_listPosicionesLibre($tipoVeh);


        return response()->json([
            'estado_operacion'=>"Exitosa",
            'data'=>$posicionesLibres
        ]);
    }

    public function listPosiciones(){
        $posicion = new Posicion;
        $posiciones=$posicion->get_allPosiciones();
        return response()->json([
            'estado_operacion'=>"Exitosa",
            'data'=>$posiciones
        ]);
    }

    public function regNewParqueo(Request $request){
        $parqueo = new Parqueo;
        $posicion = new Posicion;
        try {
            DB::beginTransaction();
            /* Se verifica posicion activa del vehiculo */
                $infoVehiculoParqueo=$parqueo->get_infoVehiculoParqueo($request->placaVeh);
                if(count($infoVehiculoParqueo)>0){
                    /* Vehiculo Activo, Operacion Denegada */
                    return response()->json([
                        'estado_operacion'=>"Denegada",
                        'data'=>"El vehiculo ya se encuentra activo"
                    ]);
                }
                $reParqueo=$parqueo->set_regNewParqueo($request);
                /* Actualizar estado de posicion */
                $updatePosicion=$posicion->set_updateEstadoPosicion($request->posicionVehiculo, "Ocupado");
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'estado_operacion'=>"Fallida",
                'data'=>$e->getMessage()
            ]);
        }
        return response()->json([
            'estado_operacion'=>"Exitosa",
            'data'=>$updatePosicion
        ]);

    }

    public function retirarVehiculo(){
        $parqueo = new Parqueo;
        $posicion = new Posicion;
        try {
            DB::beginTransaction();
            $idPosicion=$_GET['idPosicion'];
            $retirarParqueo=$parqueo->set_retirarParqueoVeh($idPosicion);
            $liberarLugar=$posicion->set_updateEstadoPosicion($idPosicion, "Libre");
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'estado_operacion'=>"Fallida",
                'data'=>$e->getMessage()
            ]);
        }

        return response()->json([
            'estado_operacion'=>"Exitosa",
            'data'=>$liberarLugar
        ]);



    }
}
