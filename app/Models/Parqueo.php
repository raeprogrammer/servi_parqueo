<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Parqueo extends Model
{
    protected $table="parqueo";


    public function get_infoParqueoPosicion($idPosicion){
        return $this->infoParqueoPosicion($idPosicion);
    }

    public function get_infoVehiculoParqueo($placaVeh){
        return $this->infoVehiculoParqueo($placaVeh);
    }

    public function set_regNewParqueo($request){
        return $this->regNewParqueo($request);
    }

    public function set_retirarParqueoVeh($idPosicion){
        return $this->retirarParqueoVeh($idPosicion);
    }

    private function infoParqueoPosicion($idPosicion){
        $infoParqueo=DB::table('parqueo')
        ->select('placaVehiculo', 'idParqueo', 'idPosicion', 'horaInicio')
        ->where('idPosicion', '=', $idPosicion)
        ->where('estado', '=', "Activo")
        ->get();
        return $infoParqueo;

    }

    private function infoVehiculoParqueo($placa){
        $infoParqueo=DB::table('parqueo')
        ->select('placaVehiculo', 'idParqueo', 'idPosicion', 'horaInicio')
        ->where('placaVehiculo', '=', $placa)
        ->where('estado', '=', "Activo")
        ->get();
        return $infoParqueo; 
    }

    private function regNewParqueo($request){
        $fechaActual=Carbon::now();
        DB::table('parqueo')
        ->insert([
            "placaVehiculo"=>$request->placaVeh,
            "idPosicion"=>$request->posicionVehiculo,
            "horaInicio"=>$request->horaInicio,
            "created_at"=>$fechaActual,
        ]);

    }

    private function retirarParqueoVeh($idPosicion){
        $fechaActual=Carbon::now();
        DB::table('parqueo')
        ->where('idPosicion', '=', $idPosicion)
        ->where('estado', '=', "Activo")
        ->update(['estado'=>"Inactivo", 'horaSalida'=>$fechaActual]);
        return "Parqueo retirado";
    }
}
