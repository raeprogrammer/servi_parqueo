<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Posicion extends Model
{
    protected $table="posiciones";


    public function get_listPosicionesLibre($tipoVeh){
        return $this->listPosicionesLibre($tipoVeh);
    }

    public function get_allPosiciones(){
        return $this->allPosiciones();
    }

    public function set_updateEstadoPosicion($idPosicion, $estado){
        return $this->updateEstadoPosicion($idPosicion, $estado);
    }

    public function set_liberarPosicion($idPosicion){
        return $this->liberarPosicion($idPosicion);
    }

    private function listPosicionesLibre($tipoVeh){
       $posiciones = DB::table('posiciones')
        ->select('idPosicion', 'tipoVehiculoPosicion', 'numPosicion', 'estado')
        ->where('tipoVehiculoPosicion', '=', $tipoVeh)
        ->where('estado', '=', "Libre")
        ->get();
        return $posiciones;
    }

    private function allPosiciones(){
        $posiciones = DB::table('posiciones')
        ->leftjoin('parqueo', 'posiciones.idPosicion', 'parqueo.idPosicion')
        ->select('posiciones.idPosicion', 'posiciones.tipoVehiculoPosicion', 'posiciones.numPosicion', 'posiciones.estado', 
        'parqueo.placaVehiculo')
        ->get();
        return $posiciones;
    }

    private function updateEstadoPosicion($idPosicion, $estado){
        DB::table('posiciones')
        ->where('idPosicion', '=', $idPosicion)
        ->update(['estado'=>$estado]);
        return "Posicion Actualizada";
    }

 
}
